﻿Chech TFN
================

|encoded|tfn                |
|-------|-------------------|
||8009956236|
|ODAwLTIwMS04OTYx|8002018961|
|ODAwLTIwMS05NjMx|8002019631|
|ODAwLTk2Ni03MDU3|8009667057|
|ODAwLTk2Ni03MDU4|8009667058|
|ODAwLTk2Ni03MTU3|8009667157|
|ODg4LTI0OS02MDI3|8882496027|
|ODg4LTI0OS02MDMw|8882496030|
|ODg4LTI0OS02MDQy|8882496042|
|ODg4LTI1OC0zMTg2|8882583186|
|ODc3LTUxNy02ODc5|8775176879|
|ODc3LTU4MS0zMjg0|8775813284|
|ODc3LTU4MS0zNjE4|8775813618|
|ODc3LTY0Ny0yNzA5|8776472709|
|ODc3LTY0Ny0zMTA3|8776473107|
|ODc3LTY0Ny0zMjUy|8776473252|
|ODU1LTMwNi00ODY5|8553064869|

Validate TFN in home and form page
--------------------------------
* Navigate to homepage "http://qa.mutualofomaha-lifeinsurance.com/" with <encoded>
* Validate <tfn>
* Go to Form Page and expect <tfn>
test