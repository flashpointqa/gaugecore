Mass Mutual Life
================

Check Post Leads
--------------------------------
* Generate "http://qalife.massmutualdirect.com/?src=test" with parameters
|parameter |value   |
|----------|--------|
| exp_landing ||
| exp_form ||
| q_publisher ||
| q_network ||
| q_creative ||
| q_criteria ||
| q_accountid ||
| q_campaignid ||
| q_adgroupid ||
| q_targetid ||
| q_feeditemid ||
| gclid ||
| q_keyword ||
| q_query ||
| q_placement ||
| q_matchtype ||
| q_adposition ||
| q_device ||
| q_devicemodel ||
| Sub_ID ||
| Pub_ID ||
| afid ||