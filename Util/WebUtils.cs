﻿using System;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Testapp;
using Gauge.CSharp.Lib;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;

namespace TestApp.Util
{
    class WebUtils : IScreenGrabber
    {
        IWebDriver _driver = Hooks.WebDriver;
        
        public static void WaitForElement(By by)
        {
            IWebDriver _driver = Hooks.WebDriver;
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            wait.Until(x => x.FindElement(by));
        }
        //Find Element
        public IWebElement FindElement(string type, string selectorvalue)
        {
            switch (type)
            {
                case "id":
                    return FindById(selectorvalue);
                case "class":
                    return FindByClass(selectorvalue);
                case "linktext":
                    return FindByLink(selectorvalue);
                case "name":
                    return FindByName(selectorvalue);
                case "tagname":
                    return FindByTagName(selectorvalue);
                case "xpath":
                    return FindByXpath(selectorvalue);
                default:
                    return FindByXpath(selectorvalue);
            }
        }
        public IWebElement FindByXpath(string xpath)
        {
            WaitForElement(By.XPath(xpath));
            return _driver.FindElement(By.XPath(xpath));
        }
        public IWebElement FindById(string id)
        {
            WaitForElement(By.Id(id));
            return _driver.FindElement(By.Id(id));
        }
        public IWebElement FindByClass(string classname)
        {
            WaitForElement(By.ClassName(classname));
            return _driver.FindElement(By.ClassName(classname));
        }
        public IWebElement FindByName(string name)
        {
            WaitForElement(By.Name(name));
            return _driver.FindElement(By.Name(name));
        }
        public IWebElement FindByTagName(string tagname)
        {
            WaitForElement(By.TagName(tagname));
            return _driver.FindElement(By.TagName(tagname));
        }
        public IWebElement FindByLink(string linkname)
        {
            WaitForElement(By.LinkText(linkname));
            return _driver.FindElement(By.LinkText(linkname));
        }
        public string getText(string type, string selectorvalue)
        {
            return FindElement(type, selectorvalue).Text;
        }
        public void VerifyTextIsTheExpected(string type, string selectorvalue, string expected)
        {
            ShouldBe(getText(type, selectorvalue), expected);
        }
        // Utils
        public Boolean ShouldBe(string value, string expected)
        {
            return value.Equals(expected);
        }

        //ScreenShoot
        public void SaveScreenShot()
        {
            Console.WriteLine(System.IO.Directory.GetCurrentDirectory());
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            MemoryStream ms = new MemoryStream(TakeScreenShot());
            Image im = Image.FromStream(ms);
            TimeAndDate.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
            string path = Path.Combine(Directory.GetCurrentDirectory(),"screenshots");
            DirectoryInfo Validation = new DirectoryInfo(path);
            string filename = TimeAndDate.ToString() + Utils.RandomString(15) + ".png";
            if (Validation.Exists == true)
            {
                im.Save(path + filename, ImageFormat.Png);
            }
            else
            {
                Validation.Create();
                im.Save(path + filename, ImageFormat.Png);
            }
            string relativepath = "../screenshots/" + filename;
            GaugeMessages.WriteMessage("<img src='../screenshots/" + filename + "'>");
        }
        public string generateRandomParameters(Table table)
        {
            string url_parameters= "";
            foreach (var item in table.GetTableRows())
            {
                if (url_parameters.Equals(""))
                {
                    if (item.GetCell("value").Equals(""))
                    {
                        url_parameters = "&" + item.GetCell("parameter") + "=" + Utils.RandomString(5);
                    }
                    else
                    {
                        url_parameters = "&" + item.GetCell("parameter") + "=" + item.GetCell("value");
                    }
                }else
                {
                    if (item.GetCell("value").Equals(""))
                    {
                        url_parameters = url_parameters  + "&" + item.GetCell("parameter") + "=" + Utils.RandomString(5);
                    }
                    else
                    {
                        url_parameters = url_parameters + "&" + item.GetCell("parameter") + "=" + item.GetCell("value");
                    }
                }

            }
            return url_parameters;
        }
        public byte[] TakeScreenShot()
        {
            return ((ITakesScreenshot)_driver).GetScreenshot().AsByteArray;
        }
        public enum Parameters
        {
            exp_landing = 11,
            exp_form = 5,
            q_publisher = 11,
            q_network = 11,
            q_creative = 11,
            q_criteria = 11,
            q_accountid = 11,
            q_campaignid = 11,
            q_adgroupid = 11,
            q_targetid = 11,
            q_feeditemid = 11,
            gclid = 11,
            q_keyword = 11,
            q_query = 11,
            q_placement = 11,
            q_matchtype = 11,
            q_adposition = 5,
            q_device = 5,
            q_devicemodel = 11,
            Sub_ID = 11,
            Pub_ID = 11,
            afid = 11
        };
    }
}
