﻿using Gauge.CSharp.Lib.Attribute;
using OpenQA.Selenium;

namespace Testapp
{
    public class Hooks
    {
        public static IWebDriver _driver;

        [BeforeSuite]
        public void Initialize()
        {
            _driver = DriverFactory.GetDriver();
            if (_driver != null)
            {
                _driver.Manage().Window.Maximize();
            }
        }
        [AfterStep]
        public void AfterStep()
        {
            
        }
        [AfterSuite]
        public void AfterSuite()
        {
            if (_driver != null)
            {
                _driver.Quit();
            }
        }
        public static IWebDriver WebDriver
        {
            get { return _driver; }
        }
    }

}