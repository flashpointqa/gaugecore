﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace Testapp
{
    public class DriverFactory
    {
        public static IWebDriver GetDriver()
        {
            var browser = Environment.GetEnvironmentVariable("BROWSER");
            switch (browser)
            {
                case "chrome":
                    return new ChromeDriver();
                case "firefox":
                    return new FirefoxDriver();
                case "ie":
                    return new InternetExplorerDriver();
                case "android":
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.AddAdditionalCapability("androidPackage", "com.android.chrome");
                    return new RemoteWebDriver(new Uri("http://127.0.0.1:9515"), chromeOptions.ToCapabilities());
                default:
                    return null;
            }
        }
    }
}
