﻿using Gauge.CSharp.Lib;
using Gauge.CSharp.Lib.Attribute;
using OpenQA.Selenium;
using System;
using Testapp;
using TestApp.Util;

namespace TestApp
{
    class CommonSteps
    {
        IWebDriver _driver = Hooks.WebDriver;
        WebUtils _wutils = new WebUtils();

        [Step("Go to website <url>")]
        public void NavigateTo(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        [Step("I send the value <parameter> to element with <type>=<selectorvalue>")]
        public void SendKeys(string parameter, string type, string selectorvalue)
        {
            _wutils.FindElement(type, selectorvalue).SendKeys(parameter);
            _wutils.SaveScreenShot();
        }
        [Step("I click the element with <type>=<selectorvalue>")]
        public void ClickTo(string type, string selectorvalue)
        {
            _wutils.FindElement(type, selectorvalue).Click();
        }
        [Step("I get text from element with <type>=<selectorvalue> and It should be <expected>")]
        public void GetText(string type, string selectorvalue, string expected)
        {
            _wutils.VerifyTextIsTheExpected(type, selectorvalue, expected);
            _wutils.SaveScreenShot();
        }
        [Step("Navigate to homepage <url> with <encoded>")]
        public void NavigateTo(string url, string encoded)
        {
            _driver.Navigate().GoToUrl(url + "?tfn=" + encoded);
        }
        [Step("Generate <url> with parameters <table>")]
        public void GenerateUrl(string url, Table table)
        {
            string url_parameters = url + _wutils.generateRandomParameters(table);
            Console.WriteLine(url_parameters);
            _driver.Navigate().GoToUrl(url_parameters);
            /*
            for (int i = 0; i < table.GetTableRows().Count; i++)
            {
                Console.WriteLine(table.ToString());
            }*/

        }
    }
}
